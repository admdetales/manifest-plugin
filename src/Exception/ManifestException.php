<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Exception;

use Throwable;
use Exception;

class ManifestException extends Exception
{
    /**
     * @var null|string
     */
    private $error;

    /**
     * ManifestException constructor.
     *
     * @param string $message
     * @param null|string $error
     * @param Throwable|null $previous
     * @param int $code
     */
    public function __construct(string $message = '', ?string $error = null, ?Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);

        $this->error = $error;
    }

    /**
     * @return null|string
     */
    public function getError(): ?string
    {
        return $this->error;
    }

}
