<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Service;


use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Repository\ShippingExportRepository;
use BitBag\SyliusShippingExportPlugin\Repository\ShippingGatewayRepository;

class ShippingExportCollector implements ShippingExportCollectorInterface
{
    /**
     * @var ShippingGatewayRepository
     */
    private $shippingGatewayRepo;

    /**
     * @var ShippingExportRepository
     */
    private $shippingExportRepo;

    /**
     * ShippingExportCollector constructor.
     * @param ShippingGatewayRepository $shippingGatewayRepo
     * @param ShippingExportRepository $shippingExportRepo
     */
    public function __construct(
        ShippingGatewayRepository $shippingGatewayRepo,
        ShippingExportRepository $shippingExportRepo
    ) {
        $this->shippingGatewayRepo = $shippingGatewayRepo;
        $this->shippingExportRepo = $shippingExportRepo;
    }

    /**
     * @param string $code
     * @return ShippingExportInterface[]
     */
    public function getShippingExports(string $code): array
    {
        $shippingGateway = $this->shippingGatewayRepo->findOneByCode($code);

        return $this->shippingExportRepo->findBy(
            [
                'shippingGateway' => $shippingGateway,
                'state' => ShippingExportInterface::STATE_EXPORTED,
            ]
        );
    }
}
