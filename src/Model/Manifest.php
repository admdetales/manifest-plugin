<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Model;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Omni\Sylius\ManifestPlugin\Constants\ShippingExportState;
use Omni\Sylius\ManifestPlugin\Model\Traits\ShippingUnitAwareTrait;
use Sylius\Component\Resource\Model\TimestampableTrait;

class Manifest implements ManifestInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var ShippingGatewayInterface
     */
    protected $shippingGateway;

    /**
     * @var ShippingExportInterface[]|Collection
     */
    protected $shippingExports;

    public function __construct()
    {
        $this->shippingExports = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): ManifestInterface
    {
        $this->path = $path;

        return $this;
    }

    public function getShippingGateway(): ShippingGatewayInterface
    {
        return $this->shippingGateway;
    }

    public function setShippingGateway(ShippingGatewayInterface $shippingGateway): ManifestInterface
    {
        $this->shippingGateway = $shippingGateway;

        return $this;
    }

    public function getShippingExports(): Collection
    {
        return $this->shippingExports;
    }

    public function addShippingExport(ShippingExportInterface $shippingExport): ManifestInterface
    {
        if (false === $this->shippingExports->contains($shippingExport)) {
            $this->shippingExports->add($shippingExport);
        }

        return $this;
    }

    public function setShippingExports(array $shippingExports): ManifestInterface
    {
        foreach ($shippingExports as $shippingExport) {
            $this->addShippingExport($shippingExport);
            $shippingExport->setState(ShippingExportState::STATE_READY);
        }

        return $this;
    }
}
